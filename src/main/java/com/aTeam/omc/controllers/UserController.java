package com.aTeam.omc.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aTeam.omc.models.ConvertRequest;
import com.aTeam.omc.models.Transaction;
import com.aTeam.omc.models.User;
import com.aTeam.omc.models.repositories.TransactionRepository;
import com.aTeam.omc.models.repositories.UserRepository;

import lombok.extern.slf4j.Slf4j;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.builder.FFmpegBuilder;

@Slf4j
@RestController
public class UserController {
	
	//private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(UserController.class);

	private final UserRepository userRepository;
	private final TransactionRepository txnRepository;

	UserController(UserRepository repository, TransactionRepository txnRepository) {
		this.userRepository = repository;
		this.txnRepository = txnRepository;
	}

	@PostMapping("/users")
	User addUser(@RequestBody User user) {
		log.info("about to add new user - "+ user);
		User persistableUser = new User(user.getName(), user.getEmail(), user.getPassword());
		return userRepository.save(persistableUser);
	}
	
	@PostMapping("/users/login")
	User loginUser(@RequestBody User user) {
		log.info("about to perform user login - " + user);
		return userRepository.findByEmailAndPassword(user.getEmail(), user.getPassword());
	}
	
	final static String TMP_OUTPUT_FOLDER = "/tmp/ffmpeg/";
	
	@PostMapping(path = "/users/{accessKey}/convert")
	ResponseEntity<Resource> convert(@PathVariable String accessKey,
				@RequestParam("inputFile") MultipartFile inputFile,
				@RequestParam("outputFormat") String outputFormat,
				 HttpServletRequest request) throws Exception {
		String INPUT_FILE_NAME = inputFile.getOriginalFilename();
		log.info("about to perform conversion op - "+ accessKey +" -- " + outputFormat + " -- "  + INPUT_FILE_NAME);
		
		User user = userRepository.findByApiKey(accessKey);
		
		String inputPath = TMP_OUTPUT_FOLDER + INPUT_FILE_NAME;
		Path path = Paths.get(inputPath);
		inputFile.transferTo(path);
		
		String OUTPUT_FILE_NAME = INPUT_FILE_NAME.substring(0, 
				INPUT_FILE_NAME.lastIndexOf(".")) + "." + outputFormat.toLowerCase();
		File outputFile = new File(TMP_OUTPUT_FOLDER + OUTPUT_FILE_NAME);
		
		log.info("OUTPUT PATH(toStr) = " + outputFile.toString());
		
		FFmpeg ffmpeg = new FFmpeg();
		FFmpegBuilder builder = new FFmpegBuilder()
				  .setInput(inputPath)     // Filename, or a FFmpegProbeResult
				  .overrideOutputFiles(true) // Override the output if it exists

				  .addOutput(outputFile.toString())   // Filename for the destination
				  .done();
		
		FFmpegExecutor executor = new FFmpegExecutor(ffmpeg);
		// Run a one-pass encode
		executor.createJob(builder).run();
		
		//File outputFile = new File(outputPath);
		InputStreamResource resource = new InputStreamResource(new FileInputStream(outputFile));
		
		// Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(outputFile.toString());
            log.info("CTYPE 1 = "+contentType);
        } catch (Exception ex) {
            log.info("Could not determine file type.");
        } 
        
        if(StringUtils.isBlank(contentType)) {
        	contentType = "application/octet-stream";
        	log.info("setting content type to default");
        }
        
        //Save transaction in DB
        Transaction txn = new Transaction(user, new Timestamp(new Date().getTime()), INPUT_FILE_NAME, outputFormat);
		txnRepository.save(txn);

	    return ResponseEntity.ok()
	    		.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + outputFile.getName() + "\"")
	            .contentLength(outputFile.length())
	            .contentType(MediaType.parseMediaType(contentType))
	            .body(resource);
	}

	@GetMapping("/users/{accessKey}/transactions")
	List<Transaction> transactions(@PathVariable String accessKey) {
		log.info("about to pull transactions for user -- " + accessKey);
		User user = userRepository.findByApiKey(accessKey);
		
		List<Transaction> transactions = new ArrayList();
		if (user != null) {
			transactions = txnRepository.findByUser(user);
		}
		return transactions;
	}
	
}
