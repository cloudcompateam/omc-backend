package com.aTeam.omc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineMediaConverterApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineMediaConverterApplication.class, args);
	}

}
