package com.aTeam.omc.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Transaction {
	@Id
	@GeneratedValue
	@Column(nullable = false)
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private User user;
	private Timestamp logDate;
	private String inputFile;
	private String outputFormat;
	
	//Constructors
	public Transaction() {};
	
	public Transaction(User user, Timestamp logDate, String inputFile, String outputFormat) {
		this.user = user;
		this.logDate = logDate;
		this.inputFile = inputFile;
		this.outputFormat = outputFormat;
	}



	//Getters & Setters
	public Long getId() {
		return id;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Timestamp getLogDate() {
		return logDate;
	}
	public void setLogDate(Timestamp date) {
		this.logDate = date;
	}
	public String getInputFile() {
		return inputFile;
	}
	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}
	public String getOutputFormat() {
		return outputFormat;
	}
	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}
	
	
}
