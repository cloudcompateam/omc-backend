package com.aTeam.omc.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aTeam.omc.models.User;

public interface UserRepository extends JpaRepository<User, Long> {
	
		public User findByEmailAndPassword(String email, String password);
		
		public User findByApiKey(String apiKey);
}

