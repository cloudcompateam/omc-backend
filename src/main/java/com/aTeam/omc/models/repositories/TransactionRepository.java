package com.aTeam.omc.models.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aTeam.omc.models.Transaction;
import com.aTeam.omc.models.User;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
	
	public List<Transaction> findByUser(User user);

}
