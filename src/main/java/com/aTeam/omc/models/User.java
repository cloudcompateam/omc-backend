package com.aTeam.omc.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.aTeam.omc.utils.Utils;

import lombok.Data;

@Data
@Entity
public class User {
	@Id
	@GeneratedValue
	@Column(nullable = false)
	private Long id;
	private String name;
	@Column(unique=true, nullable=false)
	private String email;
	private String password;

	// api credentials
	@Column(unique=true, nullable=false)
	private String apiKey;
	private String apiSecret;

	// Constructors
	User(){};
	
	public User(String name, String email, String password) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.apiKey = Utils.generateRandom();
		this.apiSecret = Utils.generateRandom();
	}

	public User(String name, String email, String password, String apiKey, String apiSecret) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.apiKey = apiKey;
		this.apiSecret = apiSecret;
	}



	// Getters & Setters
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getApiSecret() {
		return apiSecret;
	}

	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}
}
