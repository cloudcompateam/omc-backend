package com.aTeam.omc.utils;


import java.sql.Timestamp;
import java.util.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.aTeam.omc.models.User;
import com.aTeam.omc.models.repositories.TransactionRepository;
import com.aTeam.omc.models.repositories.UserRepository;
import com.aTeam.omc.models.Transaction;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class LoadDatabase {
  
  @Bean
  public CommandLineRunner initDatabase(UserRepository repository, TransactionRepository txnRepository) {
    return args -> {
      User user = new User("Bilbo Baggins", "burglar@gmail.com","pashjkalk");
      log.info("Preloading " + repository.save(user));
      log.info("Preloading " + txnRepository.save(new Transaction(user, new Timestamp(new Date().getTime()),"pacman.mpeg","AVI")));
      
    };
  }
}