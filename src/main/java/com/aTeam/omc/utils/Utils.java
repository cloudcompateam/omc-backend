package com.aTeam.omc.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class Utils {

	public static String generateRandom() {
	    return RandomStringUtils.randomAlphabetic(20);
	}

}
