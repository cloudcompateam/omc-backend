**Online Media Conversion (OMC) service**

A simple API service to allow users to **convert media files** (*image, audio,video*) from one format to another (e.g: *mp3 to aac/ogg*, *jpeg to png etc*)

The focus of this project is to create a microservice application that can be **easily scaled out and in** based on demand/load

---

## Objectives

1. Create a *cloud application* / *microservice* setup
2. Demonstrate ability to [vertically scale out and in](https://bitbucket.org/cloudcompateam/omc-orchestration-scripts.git) based on load
3. [Orchestrate the setup/deployment and auto-scaling](https://bitbucket.org/cloudcompateam/omc-orchestration-scripts.git) of the backend API service.


## Tools Used
1. Java (Spring Framework)
2. MySQL
3. FFMPEG
4. OpenStack 
5. OpenStack Orchestration Template(Heat)